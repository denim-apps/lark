import frappe
import frappe.utils.oauth
import urllib
import requests

__version__ = '0.0.1'

@frappe.whitelist(allow_guest=True)
def login():
  lark_settings = frappe.get_doc('Lark Settings')
  redirect_url = frappe.utils.get_url('/api/method/lark.login_callback')
  redirect_url = urllib.parse.quote_plus(redirect_url)

  frappe.local.response['type'] = 'redirect'
  frappe.local.response['location'] = 'https://open.larksuite.com/open-apis/authen/v1/index?redirect_uri=' + redirect_url + '&app_id=' + lark_settings.app_id
  return

@frappe.whitelist(allow_guest=True)
def login_callback():
  lark_settings = frappe.get_doc('Lark Settings')
  app_access_token = lark_settings.get_app_access_token()
  code = frappe.local.request.args.get('code')
  r = requests.post('https://open.larksuite.com/open-apis/authen/v1/access_token', json={
    'app_access_token': app_access_token,
    'grant_type': 'authorization_code',
    'code': code,
  })
  r = r.json()

  if (r['code'] == 0):
    user = r['data']
    
    # Fill in sub field for OAuth compliance
    user['sub'] = user['open_id']
    user['gender'] = ''

    frappe.utils.oauth.login_oauth_user(user, provider='lark', state={
      'token': user['access_token']
    })
    return

  return r
